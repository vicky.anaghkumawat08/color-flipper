const colors = ["green", "red", "rgba(133,122,200)", "#f15025"];
const btn1 = document.getElementById("btn1");
const color = document.querySelector(".color");
const btn2 = document.getElementById("btn2");

btn1.addEventListener("click", function() {
    const randomNumber = getRandomNumber();
    document.body.style.backgroundColor = colors[randomNumber];
    color.textContent = colors[randomNumber];
});

btn2.addEventListener("click", function() {
    document.getElementById("btn1").style.color = "white";
    const randomNumber = getRandomNumber();
    document.getElementById("btn1").style.backgroundColor = colors[randomNumber];
    color.textContent = colors[randomNumber];
});

function getRandomNumber() {
    return Math.floor(Math.random() * colors.length);
}