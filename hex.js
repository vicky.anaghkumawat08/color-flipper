const hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];
const btn1 = document.getElementById("btn1");
const color = document.querySelector(".color");
const btn2 = document.getElementById("btn2");

btn1.addEventListener("click", function(){
   let hexColor = '#';
   for(let i = 0; i <6; i++){
       hexColor += hex[getRandomNumber()];
   }
   document.body.style.backgroundColor = hexColor;
   color.textContent = hexColor;
});

btn2.addEventListener("click", function(){
   document.getElementById("btn1").style.color = "white";
   let hexColor = '#';
    for(let i = 0; i <6; i++){
        hexColor += hex[getRandomNumber()];
    }
    document.getElementById("btn1").style.backgroundColor = hexColor;
    color.textContent = hexColor;
});

function getRandomNumber(){
    return Math.floor(Math.random() * hex.length);
}
